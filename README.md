# Portfolio Website Circuit Theme

Static website built with jQuery and Bootstrap CSS.

## Desktop View
![Desktop demo](demo/resume-home-desktop.gif)

## Mobile View
![Mobile demo](demo/resume-home-mobile.gif)
